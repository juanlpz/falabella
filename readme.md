# Prueba para Front-end Developers

## Descripción

Desarrollo de una web responsiva basada en un diseño conceptual entregado por el evaluador ([ver diseño](https://cdn.dribbble.com/users/1998175/screenshots/6317545/nyc_dribbble.gif))

## Tecnologías usadas

Para el desarrollo del proyecto se utilizaron las siguientes tecnologías:

1. HTML5
2. CSS3
3. Javascript
4. Adobe Illustrator (para la creación de assets)

## Estructura del proyecto
```bash
.
├── images
│   ├── 1x
│   │   ├── puente-8.png
│   │   └── puente-80.jpg
│   └── SVG
│       ├── building.svg
│       ├── footer.svg
│       ├── nyc.svg
│       └── phone.svg
├── index.html
├── readme.md
├── scripts
│   └── index.js
└── styles
    ├── components
    │   ├── buttons.css
    │   └── input.css
    ├── effects.css
    ├── header.css
    ├── index.css
    ├── responsive
    │   ├── phones
    │   │   ├── galaxy.css
    │   │   ├── iphone.css
    │   │   └── pixel.css
    │   └── tablets
    │       ├── galaxy.css
    │       ├── ipad.css
    │       └── nexus.css
    ├── section-1.css
    ├── section-2.css
    ├── section-3.css
    └── section-4.css
```

1. Images:  contiene las imágenes usadas en el proyecto
2. index.html: código HTML del proyecto
3. scripts: contiene el código para los efectos durante el scroll
4. styles: contiene los archivos css
    1. components: contiene los estilos de botones e inputs
    2. responsive: contiene las media queries

## ¿Como correr el proyecto?
Basta con abril el archivo index.html en el chrome para ver el proyecto.

## Dispositivos
Probado en Google Chrome con los siguientes dispositivos:

- Laptop with HiDPI screen
- Laptop with MDPI screen
- Nexus 10
- Nexus 7
- Galaxy S5
- Pixel 2
- Pixel 2 Xl
- iPhone 6/7/8
- iPhone 6/7/8 Plus
- iPhone X
- iPad
- iPad Pro