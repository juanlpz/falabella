  var topPosition = $( "#page-wrap" ).scrollTop()
  var sectionNames = ['#section-one', '#section-two', '#section-three', '#section-four']
  var sections = []

  sections.push($('#section-one')[0].getBoundingClientRect().y + topPosition -100)
  sections.push($('#section-two')[0].getBoundingClientRect().y + topPosition)
  sections.push($('#section-three')[0].getBoundingClientRect().y + topPosition)
  sections.push($('#section-four')[0].getBoundingClientRect().y + topPosition)

  function sectionIndex(position) {
    for(let i = 0; i < sections.length - 1; i++) {
      if (position >= sections[i] + 100 && position < sections[i + 1] - 100) {
        return i
      }
    }
    if (position >= sections[sections.length - 1] + 40) {
        return sections.length - 1
      }
    return null
  }

  function animatedScroll1(index) {    }

  function animatedScroll(index) {
    let isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
      if(!isMobile && false) {
      let target = $(sectionNames[index]);
      $('html, body, #page-wrap').animate({scrollTop: sections[index] - 60}, 500, function() {});
    }
  }

  function addEffectsOnScroll(position) {
    if(position >= sections[1] - 100) {
      let element = $("#swing-1");
      element.addClass("swing-top-fwd-1")
      
      element = $("#swing-2");
      element.addClass("swing-top-fwd-2")

      let btns = $(".btn-sec-2")
      btns.addClass("scale-in-center")

      let items = $(".item-sec-2")
      items.addClass("scale-in-center")
      
      let icon = $("#icon-with-effects")
      icon.addClass("rotate-center");
      
    }
    if(position >= sections[2] - 100) {
      let boxes = $(".box");
      boxes.addClass("swing-top-fwd-1");
      let boxesContent = $(".box-header, .box-content, .box-content, .box-footer");
      boxesContent.addClass("swing-top-fwd-2");
    

      let btns = $(".btn-sec-3")
      btns.addClass("scale-in-center")
    }
    if(position >= sections[3] - 100) {
      let inputs = $("input");
      inputs.addClass("fade-in-left");
    }
  }

  
  var position = $( "#page-wrap" ).scrollTop()
  var isScrolling
  $("#page-wrap").scroll(function() {  
    let value = sectionIndex( $( "#page-wrap" ).scrollTop() )
    let scroll = $("#page-wrap").scrollTop();
    addEffectsOnScroll(position)
    if (value !== null ) {
      let index = 0;
      if(scroll > position ) {
        index = value + 1 > sections.length - 1 ? 2: value + 1        
      } else if(scroll < position ) {
        index = value - 1 < 0 ? 0: value       
      }
      window.clearTimeout( isScrolling );
        isScrolling = setTimeout(function() {
          animatedScroll(index)
        }, 500);
    }
    position = scroll
  });

  
